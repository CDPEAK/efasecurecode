# References:
# https://docs.python.org/2/library/logging.html
# https://docs.python.org/2/library/logging.handlers.html

# NOTE: You need to run "sudo mkdir /var/log/logging-exercise; sudo chown $USER:$USER /var/log/logging-exercise"
# before running this program (to create the log directory with the proper permissions).

import logging
from logging.handlers import RotatingFileHandler
import subprocess
import time

FORMAT_STRING = '[%(filename)s:%(lineno)s:%(funcName)s] %(message)s'

logging.basicConfig(format=FORMAT_STRING)

logger = logging.getLogger('root')

rotating_file_handler = RotatingFileHandler('/home/mtaylor/efasecurecode/logging-exercise/logging-exercise.log', maxBytes=256, backupCount=10)
rotating_file_handler.setFormatter(logging.Formatter(fmt=FORMAT_STRING))
logger.addHandler(rotating_file_handler)

logger.setLevel(logging.DEBUG)

while True:
    logger.info(subprocess.check_output('date').strip())
    time.sleep(2)
