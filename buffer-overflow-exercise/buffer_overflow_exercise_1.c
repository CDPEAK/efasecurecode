#include <stdio.h>

void main(int argc, char **argv)
{
    char username[20];
    char password[20];
    char hostname[20] = "yourcompany.com"; //our trusted domain name!

    printf("***********************************************\n");
    printf("* Derpy's ULTRA-SECURE Remote-Access Terminal *\n");
    printf("* !! FOR DERPYSOFT, INC. EMPLOYEE USE ONLY !! *\n");
    printf("***********************************************\n");

    printf("\nEnter your username: ");
    scanf("%s", username);

    //TO DO: Disable echoing of password. ;-)
    printf("Enter your password: ");
    scanf("%s", password);

    printf("\nConnecting to %s as %s...\n", hostname, username);

    //TO DO: Connect to hostname at this point.
}
