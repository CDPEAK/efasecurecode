#include <stdio.h>
#include <string.h>

void main(int argc, char **argv)
{
    char username[20];
    char password[20];
    char hostname[20] = "yourcompany.com"; //our trusted domain name!

    printf("***********************************************\n");
    printf("* Derpy's ULTRA-SECURE Remote-Access Terminal *\n");
    printf("* !! FOR DERPYSOFT, INC. EMPLOYEE USE ONLY !! *\n");
    printf("***********************************************\n");

    printf("\nEnter your username: ");
    fgets(username, sizeof(username), stdin); //will only read up to 20 characters (including the null delimiter)
    if (username[strlen(username) - 1] == '\n') username[strlen(username) - 1] = '\0'; //we don't want the newline

    //TO DO: Disable echoing of password. ;-)
    printf("Enter your password: ");
    fgets(password, sizeof(password), stdin);
    if (password[strlen(password) - 1] == '\n') password[strlen(password) - 1] = '\0'; //we don't want the newline

    printf("\nConnecting to %s as %s...\n", hostname, username);

    //TO DO: Connect to hostname at this point.
}
