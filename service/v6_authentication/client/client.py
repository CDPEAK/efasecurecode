#!/usr/bin/python

import socket, ssl, pprint

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ssl_sock = ssl.wrap_socket(s,
                           ssl_version=ssl.PROTOCOL_SSLv23,
                           ca_certs="server.crt",
                           cert_reqs=ssl.CERT_REQUIRED)

ssl_sock.connect(('localhost', 8888))

my_input = None

while my_input != 'exit':
    data = ssl_sock.recv(10000)
    print data

    if 'Error:' in data:
        break

    my_input = raw_input() or ' '
    ssl_sock.write(my_input)

ssl_sock.close()
