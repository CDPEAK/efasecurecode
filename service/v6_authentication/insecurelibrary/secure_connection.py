#!/usr/bin/python

from time import sleep
from threading import Thread
import socket
import sys
import subprocess
import ssl

class SecureConnection(object):
    def __init__(self, host, port):
        self.host, self.port = (host, port)
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 0)

        try:
            self.my_socket.bind((host, port))
        except socket.error as msg:
            print 'Bind failed. Error: %s' % msg.strerror
            sys.exit


    def wait_for_connection(self):
        self.my_socket.listen(10)

        print 'Socket now listening on port %s' % self.port


        print 'Waiting for accept'
        self.connection, self.client_address = self.my_socket.accept()
        print 'Securing connection'

        self.secure_socket = ssl.wrap_socket(self.connection,
                                            server_side=True,
                                            ssl_version=ssl.PROTOCOL_SSLv23,
                                            certfile="server.crt",
                                            keyfile="server.key")

    def prompt_user_with_text(self, text):
        self.send_user_text(text)
        return self.secure_socket.recv(100)

    def send_user_text(self, text):
        self.secure_socket.send(text)

    def close(self):   
        try:
            self.connection.close()
        except AttributeError as msg:
            pass

        self.secure_socket.close()
