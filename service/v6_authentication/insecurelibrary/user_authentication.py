#!/usr/bin/python

import MySQLdb

class UserAuthentication(object):
    def __init__(self):
        self.database_manager = MysqlDatabaseManager()

    def verify_credentials(self, username, password): 
        sql_username_and_password_query = "SELECT COUNT(*) FROM Users WHERE username ='{0}' AND password='{1}'".format(username, password)
        query_result = self.database_manager.execute(sql_username_and_password_query)

        if not query_result:
            return False
        else:
            if query_result[0] > 0:
                return True
            else:
                return False

    def connect(self, hostname, username, password, database):
        self.database_manager.connect(hostname, username, password, database)

class MysqlDatabaseManager(object):
    def connect(self, hostname, username, password, database):
        conn = MySQLdb.connect(host=hostname, user=username, passwd=password, db=database)
        self._database = conn.cursor()

    def execute(self, sql_command):
        self._database.execute(sql_command)
        return self._database.fetchone()

if __name__ == '__main__':
    user_authentication = UserAuthentication()
    user_authentication.connect('localhost', 'root', 'password', 'service')
    print user_authentication.verify_credentials("megatron","123415") 
