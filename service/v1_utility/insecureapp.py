#!/usr/bin/python

from time import sleep
import socket
import sys
import subprocess

s_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn_main = None

def start():
    listen_for_connections()
    if conn_main:
        conn_main.close()
    if s_main:
        s_main.close()
    print 'Exiting...'

def listen_for_connections():
    host = ''

    s_main.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s_main.bind((host, 8888))

    s_main.listen(10)
    print 'Socket now listening on port %s' % 8888

    conn_main, addr = (None, None)

    print 'Waiting for accept'
    conn_main, addr = s_main.accept()
    print 'Handling new connection'
    print 'Connected with ' + addr[0] + ':' + str(addr)
    conn_main.send("Running ifconfig...")
    results = subprocess.Popen("ifconfig", shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE).stdout.read()
    conn_main.send(results)
    print 'Disconnected with ' + addr[0] + ':' + str(addr)

start()
