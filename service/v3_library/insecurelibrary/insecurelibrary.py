#!/usr/bin/python

from time import sleep
import socket
import sys
import subprocess

class AdminServer(object):
    def __init__(self, command, prompt):
        self.command = command
        self.prompt = prompt

        self.s_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn_main = None

    def start(self):
        threads = []
        try:
            self.listen_for_standard_connections()
        except KeyboardInterrupt:
            print 'Ctrl C pressed...'
            print 'Closing socket...'
            try:
                self.conn_main.close()
            except AttributeError as msg:
                pass
            self.s_main.close()
            self.s_main = None
            print 'Socket closed'
            print 'Exiting...'

    def run_command(self, parameters):
        results = ""
        statement = self.command + " " + parameters

        results = subprocess.Popen(statement, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE).stdout.read()

        return results

    def listen_for_connections(self, port, my_socket, my_connection, command):
        host = ''

        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            my_socket.bind((host, port))
        except socket.error as msg:
            print 'Bind failed. Error: %s' % msg.strerror
            sys.exit

        my_socket.listen(10)
        print 'Socket now listening on port %s' % port

        my_connection, addr = (None, None)

        while my_socket:
            try:
                print 'Waiting for accept'
                my_connection, addr = my_socket.accept()
                print 'Handling new connection'
                if my_connection:
                    parameters = ''
                    while parameters != 'exit':
                        print 'Connected with ' + addr[0] + ':' + str(addr)
                        my_connection.send(self.prompt + ", or type 'exit' to quit: ")
                        parameters = my_connection.recv(100)
                        parameters = parameters.strip()
                        if parameters != 'exit':
                            results = command(parameters)
                            my_connection.send(results)
            except socket.error as msg:
                print 'Connection failed. Error: %s' % msg.strerror
                pass
            finally:
                try:
                    my_connection.close()
                except:
                    pass
                if addr:
                    print 'Disconnected with ' + addr[0] + ':' + str(addr)

    def listen_for_standard_connections(self):
        self.listen_for_connections(8888, self.s_main, self.conn_main, self.run_command)

class AdminIfconfigServer(AdminServer):
    def __init__(self):
        super(AdminIfconfigServer, self).__init__("ifconfig", "Please enter an interface name")

if __name__ == '__main__':
    server = AdminIfconfigServer()
    server.start()
