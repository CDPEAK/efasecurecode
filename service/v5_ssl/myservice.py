#!/usr/bin/python

from insecurelibrary.insecurelibrary import AdminServer

class AdminNetstatServer(AdminServer):
    def __init__(self):
        super(AdminNetstatServer, self).__init__("netstat -ntlp | grep ", "Press enter to run netstat")

    def run_command(self, parameters):
        return super(AdminNetstatServer, self).run_command("'" + parameters + "'")

if __name__ == '__main__':
    server = AdminNetstatServer()
    server.start()                    
